from django.contrib import admin
from . import models

@admin.register(models.Door)
class DoorAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Card)
class CardAdmin(admin.ModelAdmin):
    list_display=['card_owner','is_root']

@admin.register(models.AuthLog)
class AuthLogAdmin(admin.ModelAdmin):
    list_display=['door','card','date_time']


