from django.urls import include, path
from door_auth.api.views import LoginView, CardListView, RootCardView

urlpatterns = [
    path('login/', LoginView.as_view()),
    path('cards/', CardListView.as_view()),
    path('root/', RootCardView.as_view())
]
