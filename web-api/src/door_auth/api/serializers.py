from door_auth.models import Door, Card, AuthLog
from rest_framework.serializers import ModelSerializer,Serializer, IntegerField, CharField

class CardSerializer(ModelSerializer):
    class Meta:
        model = Card
        fields = ('card_id','card_owner')

class AuthLogPostSerializer(Serializer):
    card_uid = CharField(max_length=20)
    door_id = IntegerField()