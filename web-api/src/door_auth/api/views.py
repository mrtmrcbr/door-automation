from door_auth.models import Card, Door, AuthLog
from .serializers import CardSerializer, AuthLogPostSerializer
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist

class LoginView(APIView):
    def post(self, request, format=None):
        serializer = AuthLogPostSerializer(data=request.data)
        print(request.data)
        if serializer.is_valid(raise_exception=True):
            door_id = serializer.validated_data['door_id']
            card_uid = serializer.validated_data['card_uid']
            try:
                door = Door.objects.get(
                    id = door_id
                )
            except ObjectDoesNotExist:
                return Response({
                    'message': 'There is not an Door with this ip',
                    },
                    status=status.HTTP_404_NOT_FOUND)
            else:
                try:
                    card = Card.objects.get(
                    card_id = card_uid
                    )
                except ObjectDoesNotExist:
                    return Response({
                        'message': 'There is not an Card with this uid',
                        },
                        status=status.HTTP_404_NOT_FOUND)
                else:
                    print(door,card)
                    try:
                        new_login = AuthLog.objects.create(door=door, card=card)
                    except Exception as e:
                        return Response({
                            'message': 'İnvalid operation',
                            },
                            status=status.HTTP_400_BAD_REQUEST
                        )
                    else:
                        return Response({
                            'message': 'Object saved into database',
                        })
                
        return Response({
                    'message': 'not',
                },
                status=status.HTTP_404_NOT_FOUND
            )

class RootCardView(APIView):
    def get(self, request, format=None):
        try:
            card = Card.objects.get(
                is_root = True
            )
        except ObjectDoesNotExist:
            return Response({
                'message': 'There is not any root card',
                },
                status=status.HTTP_404_NOT_FOUND)
        else:
            serialized_card = CardSerializer(card)
            return Response(serialized_card.data)

class CardListView(ListAPIView):
    queryset = Card.objects.all()
    serializer_class = CardSerializer