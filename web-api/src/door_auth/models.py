from django.db import models

class Card(models.Model):
    card_owner = models.CharField(max_length=255)
    card_id = models.CharField(max_length=11)
    is_root = models.BooleanField(default=False)

    def __str__(self):
        return self.card_owner

class Door(models.Model):
    name = models.CharField(max_length=255)
    ip = models.CharField(max_length=20)
    cards = models.ManyToManyField(Card)

    def __str__(self):
        return self.name

class AuthLog(models.Model):
    door = models.ForeignKey(Door,on_delete=models.CASCADE)
    card = models.ForeignKey(Card,on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now=True)
