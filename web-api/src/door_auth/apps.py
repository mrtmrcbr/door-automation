from django.apps import AppConfig


class DoorAuthConfig(AppConfig):
    name = 'door_auth'
