#include <SPI.h>
#include <MFRC522.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <Arduino_JSON.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>

#define SS_PIN 4
#define RST_PIN 5

const byte granted_led_pin = 16;
const byte denied_led_pin = 0;
const byte root_led_pin = 2;

const byte max_card_count = 5;
const String device_id = "1";

String root_card_uid;
String web_api_path = "http://192.168.1.3:8000/api/";

const char* ssid = "VODAFONE_0364";
const char* password = "5bf27gc4b954u475";

MFRC522 rfid(SS_PIN, RST_PIN);
ESP8266WiFiMulti wifi_multi;
JSONVar granted_cards;

void flash_led(byte led_pin = 0, short int delay_interval = 1000) {
  digitalWrite(led_pin, HIGH);
  delay(delay_interval);
  digitalWrite(led_pin, LOW);

}

String send_get_request(String path) {
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;

    Serial.println("[HTTP] begin...");
    if (http.begin(path)) {
      Serial.print("[HTTP] GET...\n");
      int response_code = http.GET();
      if (response_code > 0) {
        Serial.printf("[HTTP] GET... code: %d\n", response_code);
        if (response_code == HTTP_CODE_OK || response_code == HTTP_CODE_MOVED_PERMANENTLY) {
          return http.getString();
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(response_code).c_str());
        return http.errorToString(response_code).c_str();
      }
      http.end();
    } else {
      Serial.println("[HTTP] Unable to connect");
      return "Unable to connect";
    }
  }
}

JSONVar parse_json(String data) {
  JSONVar parsed_data = JSON.parse(data);
  if (JSON.typeof(parsed_data) == "undefined") {
    Serial.println("[JSON] Parsing input failed!");
    return JSON.parse("{\"message\":\"json object is not serializable\"}");
  }
  return parsed_data;
}

String convert_hex_to_uid(byte *buffer, byte bufferSize) {
  String uid = "";

  for (byte i = 0; i < bufferSize; i++)
  {
    uid.concat(String(buffer[i] < 0x10 ? " 0" : " "));
    uid.concat(String(buffer[i], HEX));
  }

  uid.toUpperCase();
  return uid.substring(1);
}

int is_root_card(String card_uid) {
  if (card_uid == root_card_uid)
    return 1;
  else return 0;
}

int is_granted_card(String card_uid) {
  for (byte card_index = 0; card_index < max_card_count; card_index++) {
    if (card_uid == granted_cards[card_index]["card_id"]) return 1;
  }
  return 0;
}

void setup() {

  pinMode(granted_led_pin, OUTPUT);
  pinMode(denied_led_pin, OUTPUT);
  pinMode(root_led_pin, OUTPUT);

  Serial.begin(9600);
  SPI.begin();
  rfid.PCD_Init();

  for (uint8_t t = 4; t > 0; t--) {
    digitalWrite(root_led_pin, LOW);
    digitalWrite(granted_led_pin, LOW);
    digitalWrite(denied_led_pin, LOW);
    Serial.printf("[SETUP] WAIT %d...\n", t);
    delay(1000);
    Serial.flush();
    digitalWrite(root_led_pin, HIGH);
    digitalWrite(granted_led_pin, HIGH);
    digitalWrite(denied_led_pin, HIGH);
    delay(1000);

  }

  WiFi.mode(WIFI_STA);
  wifi_multi.addAP(ssid, password);

  String response_data = send_get_request(web_api_path + "root/");
  JSONVar root_card = parse_json(response_data);
  root_card_uid = root_card["card_id"];

  Serial.println(F("[SETUP] Done."));
  
  digitalWrite(root_led_pin, LOW);
  digitalWrite(denied_led_pin, LOW);
  digitalWrite(granted_led_pin, LOW);
  delay(2000);
  
  Serial.println(F("[INFO] If you want to refresh The accessed card ids, please use root card"));
}

void loop() {

  if ( ! rfid.PICC_IsNewCardPresent())
    return;

  if ( ! rfid.PICC_ReadCardSerial())
    return;

  String card_uid = convert_hex_to_uid(rfid.uid.uidByte, rfid.uid.size);

  if ( is_root_card(card_uid) ) {
    String response_data = send_get_request(web_api_path + "cards/");
    granted_cards = parse_json(response_data);
    flash_led(root_led_pin);

  }
  else {
    if ( is_granted_card(card_uid) ) {

      flash_led(granted_led_pin);
    }
    else {

      flash_led(denied_led_pin);
    }
  }

  rfid.PICC_HaltA();
  rfid.PCD_StopCrypto1();
}
