## Instructions

Note: Make sure you are running first web-api

### Web
- Clone or download the repo

- You must have python3 and pip in your system.

- run web-api

```sh
pip install virtualenv

cd door_automation/web-api

virtualenv venv
```
- **for Gnu/linux systems**
```sh
source venv/bin/activate
```

- **for Windows systems**
```sh
\venv\Scripts\activate.bat
```

- After activating environment install python dependecies and run web server

```sh
pip install -r requirements.txt

python src/manage.py createsuperuser

python src/manage.py runserver 0.0.0.0:8000
```

### Arduino

- open arduino code with arduino IDE 

- if you dont have esp8266 related packages first install its dependecies;

    - `http://arduino.esp8266.com/stable/package_esp8266com_index.json` paste that link under preferences into `Additional Boards Manager Urls` section.

    - open boards manager, it is in tools section search for `esp8266` and install comminity version.

- install arduino_json package from packager manager.

- compile and upload

